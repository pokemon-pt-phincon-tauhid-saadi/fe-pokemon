import Vue from 'vue'
import Vuex from 'vuex'
import InfiniteLoading from 'vue-infinite-loading';

Vue.use(Vuex)
Vue.use(InfiniteLoading)

const BASE_URL = 'https://pokeapi.co/api/v2';
const BASE_URL_SCRAP = 'http://localhost:3333';

const API = `${BASE_URL}/generation/1/`;

export default new Vuex.Store({
  state: {
    pokemons: [],
    myPokemons: [],
    pokemonDetail: {},
    region: null,
    generationPokemons: [],
    dinamicRegion: null
  },
  mutations: {
    GET_POKEMONS (state, pokemons) {
      state.pokemons = pokemons
    },
    GET_MY_POKEMONS (state, myPokemons) {
      state.myPokemons = myPokemons
    },
    REGION (state, region) {
      state.region = region
    },
    DETAIL_POKEMON (state, detail) {
      state.pokemonDetail = detail
    },
    GET_GENERATION_POKEMONS (state, generation) {
      state.generationPokemons = generation
    },
    REGION_NAMES (state, region) {
      state.dinamicRegion = region
    }
    
  },
  actions: {
    getPokemons ({commit}) {
      return fetch(API)
        .then( res => {
          if(res.status === 200)
            return res.json()
        })
        .then( data => {
          let region = data.main_region.name
          // acción payload que evita traer nuevamente la misma data al llamar la función
          commit('GET_POKEMONS',
            data.pokemon_species.map(pokemon => {
              pokemon.region = region
              pokemon.id = pokemon.url.split('/')
                .filter(part => {return !!part}).pop()
              return pokemon // retorna los elementos del mapeo con la nueva data
            })
          )
          
        })
        .catch( error => console.log(error) )
    },
    async nameRegion ({ commit }) {
      try {
        let api = await fetch(API)
        let res = await api.json()
        let data = await commit('REGION', res.main_region.name)
        return data
      } catch (rej) {
        console.error(rej)
      }
    },
    async detailPokemon ({ commit }, idPoke) {
      try {
        let api = await fetch(`${BASE_URL}/pokemon/${idPoke}/`)
        let res = await api.json()
        let data = await commit('DETAIL_POKEMON', res)
        return data
      } catch (rej) {
        console.error(rej)
      }
    },
    async getGenerationPokemons ({commit}, idGeneration) {
      try {
        let api = await fetch(`${BASE_URL}/generation/${idGeneration}/`)
        let res = await api.json()
        let region = await res.main_region.name
        let data = await commit('GET_GENERATION_POKEMONS', 
          res.pokemon_species.map(pokemon => {
            pokemon.region = region
            pokemon.id = pokemon.url.split('/')
            .filter(part => {return !!part}).pop()
            return pokemon
          })
        )
        return data
      } catch (rej) {
        console.log(rej)
      }
    },
    async regionNames ({commit}, idRegion) {
      try {
        let api = await fetch(`${BASE_URL}/generation/${idRegion}/`)
        let res = await api.json()
        let data = await commit('REGION_NAMES', res.main_region.name)
        return data
      } catch (rej) {
        console.log(rej)
      }
    },
    
    getMyPokemons ({commit}) {
      return fetch(API)
        .then( res => {
          if(res.status === 200)
            return res.json()
        })
        .then( data => {
          let region = data.main_region.name
          commit('GET_MY_POKEMONS',
            data.pokemon_species.map(pokemon => {
              pokemon.region = region
              pokemon.id = pokemon.url.split('/')
                .filter(part => {return !!part}).pop()
              return pokemon // retorna los elementos del mapeo con la nueva data
            })
          )
          
        })
        .catch( error => console.log(error) )
    },
    async scrapPokemon() {
      try {
        let api = await fetch(`${BASE_URL_SCRAP}/pokemon/check/probability`)
        let res = await api.json()
        return res
      } catch (rej) {
        alert(rej)
        console.error(rej)
      }
    },
    async releasePokemon({commit}, id) {
      try {
        console.log('params', id)
        let api = await fetch(`${BASE_URL_SCRAP}/pokemon/check/primernumber/${id}`)
        let res = await api.json()
        return res
      } catch (rej) {
        alert(rej)
        console.error(rej)
      }
    },
    async renamePokemon({commit}, data) {
      try {
        const config = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        };
        let api = await fetch(`${BASE_URL_SCRAP}/pokemon/do/rename/`, config)
        let res = await api.json()
        return res
      } catch (rej) {
        alert(rej)
        console.error(rej)
      }
    }
  },
  modules: {
  }
})
